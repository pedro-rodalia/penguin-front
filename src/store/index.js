import Vue from 'vue'
import Vuex from 'vuex'
import modules from './modules'
import api from '@/store/services'

api.init(process.env.VUE_APP_BASE_URL)
api.use('auth', { endpoint: '/api/application/auth' })

Vue.use(Vuex)

export default new Vuex.Store({ modules })
